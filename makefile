down:
	docker-compose down

up:
	docker-compose up

makemigrations:
	docker-compose exec web python manage.py makemigrations

migrate:
	docker-compose exec web python manage.py migrate

shell:
	docker-compose exec web python manage.py shell

createsuperuser:
	docker-compose exec web python manage.py createsuperuser --username admin --email admin@localhost

delmigrations:
	find . -path "*/migrations/*.py" -not -name "__init__.py" -delete

reinit:
	docker-compose down -v #drop tables
	docker-compose up -d #run docker
	find . -path "*/migrations/*.py" -not -name "__init__.py" -delete #dellmigrations
	docker-compose exec web python manage.py makemigrations
	docker-compose exec web python manage.py migrate
	docker-compose exec web python manage.py createsuperuser --username admin --email admin@localhost

init:
	docker-compose up -d #run docker
	find . -path "*/migrations/*.py" -not -name "__init__.py" -delete #dellmigrations
	docker-compose exec web python manage.py makemigrations
	docker-compose exec web python manage.py migrate
	docker-compose exec web python manage.py createsuperuser

runtests:
	docker-compose exec web python manage.py test