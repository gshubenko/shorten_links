from django.urls import path
from rest_framework import routers

from .views import GenerateLinkViewSet

router = routers.SimpleRouter()
router.register("link", GenerateLinkViewSet, basename="link")


urlpatterns = router.urls
# urlpatterns = [
#     path("generate_url/", GenerateLinkView.as_view(), name="generate_url")
# ]
