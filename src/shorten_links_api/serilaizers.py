
from urllib.parse import urlunsplit
from rest_framework import serializers
from drf_yasg.utils import swagger_serializer_method

from .models import LinkModel


class LinkSerializer(serializers.ModelSerializer):
    short_link = serializers.SerializerMethodField()
    expire_at = serializers.SerializerMethodField()
    link = serializers.URLField()

    @swagger_serializer_method(serializer_or_field=serializers.DateTimeField)
    def get_expire_at(self, obj):
        return obj.expire_at

    @swagger_serializer_method(serializer_or_field=serializers.URLField)
    def get_short_link(self, obj: LinkModel):
        link_code = obj.link_code
        scheme = self.context["request"].scheme
        host = self.context["request"].get_host()
        return urlunsplit((scheme, host, link_code, {}, ""))

    class Meta:
        model = LinkModel
        fields = ("id", "user", 'link', "expire", "short_link", "expire_at")


class LinkBodySerializer(serializers.ModelSerializer):

    class Meta:
        model = LinkModel
        fields = ('link', "expire",)
