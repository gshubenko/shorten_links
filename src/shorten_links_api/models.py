from datetime import timedelta

from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth import get_user_model


from .services import base_n_encode


class LinkModel(models.Model):
    link = models.URLField(verbose_name=_('link'), null=True, blank=True)
    expire = models.IntegerField(
        default=90,
        validators=(MinValueValidator(1), MaxValueValidator(365)),
        verbose_name=_("Expire")
    )
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE,
                             blank=True, null=True, verbose_name=_("user"))

    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name=_("Created at"))
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name=_("Updated at"))

    @property
    def link_code(self):
        if not self.id is None:
            return base_n_encode(self.id)

    @property
    def expire_at(self):
        return (timezone.now() + timedelta(days=self.expire))\
            .astimezone(timezone.get_current_timezone())

    @property
    def is_expire(self):
        return self.created_at > self.expire_at

    def __str__(self) -> str:
        return f"Link({self.id})"

    class Meta:
        verbose_name = _('Link')
        verbose_name_plural = _('Links')
