from datetime import datetime, timedelta

from django.shortcuts import get_object_or_404
from django.db.models import Q, F
from django.utils import timezone
from django.conf import settings

ALPHABET = settings.ALPHABET


def base_n_encode(number, alphabet=ALPHABET) -> str:
    """Base N encoder for integer"""
    if not isinstance(number, (int)):
        raise TypeError('number must be an integer')
    is_negative = number < 0
    number = abs(number)

    alphabet, base_n = [alphabet, '']

    while number:
        number, i = divmod(number, len(alphabet))
        base_n = alphabet[i] + base_n
    if is_negative:
        base_n = '-' + base_n

    return base_n or alphabet[0]


def base_n_decode(number, alphabet=ALPHABET) -> int:
    """Base N decoder for base n. 
    Alphabet should be the same with encoder"""
    number = str(number)
    alphabet_len = len(alphabet)
    try:
        indexes = sum([alphabet.index(num) * pow(alphabet_len, indx)
                    for indx, num in enumerate(number[::-1])])
    except ValueError:
        return None
    return indexes


def find_link(link_code, queryset):
    link_id = base_n_decode(link_code)
    return get_object_or_404(queryset, pk=link_id)


def get_free_link(queryset):
    link = queryset.filter(
        Q(link__isnull=True) | Q(
            created_at__lt=timezone.now()-timedelta(days=1)*F("expire"))
    ).first()
    return link
