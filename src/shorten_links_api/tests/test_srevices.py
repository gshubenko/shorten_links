import random

from rest_framework.test import APITestCase

from ..services import base_n_encode, base_n_decode



class TestEncoderDecoder(APITestCase):
    def setUp(self) -> None:
        self.alphabet_base36 = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        self.alphabet_base_n = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
        self.randomlist = random.sample(range(10, 10000), 5)
    
    def test_encoder(self):
        """Test encoder work correctly"""
        
        #Test in base 36
        self.assertEqual(base_n_encode(5, alphabet=self.alphabet_base36), "5")
        self.assertEqual(base_n_encode(36, alphabet=self.alphabet_base36), "10")
        self.assertEqual(base_n_encode(1000, alphabet=self.alphabet_base36), "RS")

    def test_decoder(self):
        """Test decoder work correctly"""
        
        #Test in base 36
        self.assertEqual(base_n_decode("5", alphabet=self.alphabet_base36), 5)
        self.assertEqual(base_n_decode("10", alphabet=self.alphabet_base36), 36)
        self.assertEqual(base_n_decode("RS", alphabet=self.alphabet_base36), 1000)


    def test_encoder_decoder(self):
        """Test encoder -> decoder work correctly"""

        #Test encode and decode in base 36
        for i in self.randomlist:
            encoded = base_n_encode(i, alphabet=self.alphabet_base36)
            decoded = base_n_decode(encoded, alphabet=self.alphabet_base36)
            self.assertEqual(i, decoded)

        #Test encode and decode in base n
        for i in self.randomlist:
            encoded = base_n_encode(i, alphabet=self.alphabet_base_n)
            decoded = base_n_decode(encoded, alphabet=self.alphabet_base_n)
            self.assertEqual(i, decoded)
