from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from shorten_links_api.models import LinkModel
from shorten_links_api.serilaizers import LinkSerializer


class AccountTests(APITestCase):
    def test_generate_link_with_default(self):
        """
        Ensure we can generate a link.
        """
        link = 'https://www.google.com.ua/'
        url = reverse('link-list')
        data = {'link': link}

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(LinkModel.objects.count(), 1)

        link_model = LinkModel.objects.get()

        self.assertEqual(link_model.link, link)
        self.assertEqual(link_model.expire, 90)
        self.assertEqual(link_model.is_expire, False)


    def test_generate_link(self):
        """
        Ensure we can generate a link.
        """
        link = 'https://www.google.com.ua/'
        url = reverse('link-list')
        data = {'link': link, "expire": 30}

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(LinkModel.objects.count(), 1)

        link_model = LinkModel.objects.get()

        self.assertEqual(link_model.link, link)
        self.assertEqual(link_model.expire, 30)
        self.assertEqual(link_model.is_expire, False)


    def test_get_link(self):
        """Test get_link"""

        link = 'https://www.google.com.ua/'
        post_url = reverse('link-list')
        data = {'link': link, "expire": 30}

        self.client.post(post_url, data, format='json')
        link_model = LinkModel.objects.get()

        get_url = reverse('get_link', args=(link_model.link_code,))
        get_response = self.client.get(get_url)
        self.assertEqual(get_response.status_code, status.HTTP_302_FOUND)
        

        get_response = self.client.get(get_url)
        self.assertEqual(get_response.status_code, status.HTTP_302_FOUND)
    

    def test_notfound_get_link(self):

        get_url = reverse('get_link', args=("AFSa",))
        get_response = self.client.get(get_url)
        self.assertEqual(get_response.status_code, status.HTTP_404_NOT_FOUND)