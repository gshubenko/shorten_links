from rest_framework.generics import GenericAPIView, CreateAPIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from django.http import HttpResponseRedirect
from rest_framework import status
from django.http import Http404
from drf_yasg.utils import swagger_auto_schema
from rest_framework import serializers

from .serilaizers import LinkSerializer, LinkBodySerializer
from .models import LinkModel
from .services import get_free_link, find_link


class GenerateLinkViewSet(ModelViewSet):
    serializer_class = LinkSerializer
    queryset = LinkModel.objects.all()

    def get_queryset(self):
        queryset = super().get_queryset()
        if self.request.user.is_anonymous:
            return queryset.none()
        return queryset.filter(user=self.request.user)

    @swagger_auto_schema(
        request_body=LinkBodySerializer, 
        responses={
        201:LinkSerializer
    })
    def create(self, request, *args, **kwargs):
        """Link for generating a short link.
        - link must be a uri
        - expire default is 90"""
        request.data['user'] = self.request.user.pk
        queryset = self.get_queryset()
        link = get_free_link(queryset)
        if not link is None:
            link.expire = 90
        serializer = self.get_serializer(instance=link, data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data,
                        status=status.HTTP_201_CREATED, headers=headers)

    @swagger_auto_schema(
        request_body=LinkBodySerializer, 
        responses={
        200:LinkSerializer
    })
    def update(self, request, *args, **kwargs):
        request.data['user'] = self.request.user.pk
        return super().update(request, *args, **kwargs)
    
    @swagger_auto_schema(
        request_body=LinkBodySerializer, 
        responses={
        200:LinkSerializer
    })
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    def perform_destroy(self, instance):
        instance.user = None
        instance.link = None
        instance.save()

class GetLinkView(GenericAPIView):
    queryset = LinkModel.objects.all()
    serializer_class = serializers.Serializer

    def get_object(self):
        queryset = self.get_queryset()
        link_code = self.kwargs["link_code"]
        instance = find_link(link_code, queryset)
        return instance

    def get(self, request, *args, **kwargs):
        """Redirect to generated link"""
        instance = self.get_object()
        if instance.is_expire:
            instance.url = None
            instance.save()
            raise Http404("Link not found")
        return HttpResponseRedirect(instance.link)