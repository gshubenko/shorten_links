from django.apps import AppConfig


class ShortenLinksApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'shorten_links_api'
