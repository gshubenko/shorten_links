from django.contrib import admin

from .models import LinkModel

@admin.register(LinkModel)
class LinkAdmin(admin.ModelAdmin):
    list_display = ("id", "link_code", "expire", "created_at", "updated_at")