from django.contrib.auth.models import User
from .serializers import RegisterSerializer
from rest_framework import generics
from rest_framework import permissions as rest_permissions



class RegisterView(generics.CreateAPIView):
    """View to create a new user"""
    queryset = User.objects.all()
    permission_classes = (rest_permissions.AllowAny,)
    serializer_class = RegisterSerializer